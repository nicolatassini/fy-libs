window.firsty = (function () {
   
    var firsty = {
      
      /**
       * URL for the Authentication
       */
      AUTHORIZE_URL : '/linkedin/login',
      CALLBACK_URL : '/build/tokenCallback.html',
      API_VERSION : '/api/v1.0/',

      GET : 'GET',
      POST : 'POST',
      DELETE : 'DELETE',
      
      /**
       * URL for the backend 
       */
      backendURL : 'https://firsty-pre.herokuapp.com',      
      setBackendURL : function(backendURL) {
        this.backendURL = backendURL;
      },
      getBackendURL : function() {
        return this.backendURL;
      },

      /**
       * URL for the frontend 
       */
      frontendURL : 'https://firstyfe-pre.herokuapp.com',
      setFrontendURL : function(frontendURL) {
        this.frontendURL = frontendURL;
      },
      getFrontendURL : function() {
        return this.frontendURL;
      },
      
      
      /**
       *  Log to the FY Backend
       *  @param callback Callback function called once oAuth dance is finished
       */
      login : function() {
    	  var deferred = $.Deferred();

    	  // Phonegap
    	  if(document.location.protocol == "file:") {
    		  var ref = window.open(this.getBackendURL() + this.AUTHORIZE_URL, '_blank', 'location=no');
    		  var _this = this;
    		  ref.addEventListener('loadstart', function(event) { 
    			  if(event.url.indexOf(_this.getFrontendURL() + _this.CALLBACK_URL) == 0) {
    				  ref.close();
    				  var _token = event.url.substring((_this.getFrontendURL() + _this.CALLBACK_URL + '?token=').length);
    				  if(_token.length > 0) {
    					  deferred.resolve(_token);
    				  } else {
    					  deferred.reject('Token not found');
    				  }
    			  }
    		  });
    	  } 
    	  // Browser
    	  else {
    		  var winHeight = 524,
    		  winWidth = 674,
    		  centeredY = window.screenY + (window.outerHeight / 2 - winHeight / 2),
    		  centeredX = window.screenX + (window.outerWidth / 2 - winWidth / 2);
    		  var loginWindow = window.open(this.getBackendURL() + this.AUTHORIZE_URL,
    				  'Login to Salesforce', 'height=' + winHeight + ',width=' + winWidth
    				  + ',toolbar=1,scrollbars=1,status=1,resizable=1,location=0,menuBar=0'
    				  + ',left=' + centeredX + ',top=' + centeredY);

    		  if (loginWindow) {
    			  // Creating an interval to detect popup window location change event
    			  var _this = this;
    			  var interval = setInterval(function () {
    				  if (loginWindow.closed) {
    					  // Clearing interval if popup was closed
    					  clearInterval(interval);
    				  } else {
    					  var loc = loginWindow.location.href;
    					  if (typeof loc !== 'undefined' && loc.indexOf(_this.getFrontendURL() + _this.CALLBACK_URL) == 0) {
    						  loginWindow.close();

    						  var _token = loc.substring((_this.getFrontendURL() + _this.CALLBACK_URL + '?token=').length);
    						  if(_token.length > 0) {
    							  deferred.resolve(_token);
    						  } else {
    							  deferred.reject('Token not found');
    						  }
    					  }
    				  }
    			  }, 250);
    			  loginWindow.focus();
    		  }
    	  }

    	  return deferred.promise();
      },

      /**
       * Set a session token
       * @param sessionId a FY session ID
       */
      setSessionToken : function(sessionToken) {
        this.sessionToken = sessionToken;
      },

      /**
       * Wrap the ajax call
       * @param url URL to call
       * @param method HTTP Method to call
       * @param paylod parameters
       */
      ajax : function(url, method, payload, contentType) {
        var that = this;
        var urlAPI = this.getBackendURL() + this.API_VERSION + url;

        return jQuery.ajax({
            type: method || this.GET,
            async: true,
            url: urlAPI,
            contentType: method == this.DELETE  ? null : 
              ( typeof contentType == 'undefined' ? 'application/json' : contentType),
            cache: false,
            processData: false,
            data: payload,
            dataType: "json",
            processData: true,
            /*
            success: callback,
            error: (!this.refreshToken || retry ) ? error : function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status === 401) {
                    that.refreshAccessToken(function(oauthResponse) {
                        that.setSessionToken(oauthResponse.access_token, null,
                        oauthResponse.instance_url);
                        that.ajax(path, callback, error, method, payload, true);
                    },
                    error);
                } else {
                    error(jqXHR, textStatus, errorThrown);
                }
            },
            */
            beforeSend: function(xhr) {
                xhr.setRequestHeader('token', that.sessionToken);
            }
        });
      },

      /**
       * Get the list of job descriptions
       */
      jobDescriptionList : function() {
        return this.ajax('jobDescriptions');
      },

      /**
       * Create a new job description
       */
      jobDescriptionCreate : function(data) {
        return this.ajax('jobDescription/edit', this.POST, data, 'application/x-www-form-urlencoded');
      }
    };
     
    return firsty;
}());